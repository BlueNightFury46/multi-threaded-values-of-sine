#include <cmath>
#include <iostream>

namespace progressbar{


//Internal lambda to round value
auto _rlambda = [](double val){

  if(val > 100){
    return 10.0;
  }

  return round((val/10))-1;
};

class progressBar{

private:
  
  static const int barLength = 10;
  char barvalue[barLength];
  int percent;

public:  

//Set the filler chars
 char EMPTY_CHAR = '~';
 char FULL_CHAR = '#';
 char LEFT_CHAR = '[';
 char RIGHT_CHAR = ']'; 
 

  //Print out the list
  void print(){

    //print out [
    printf("%c", LEFT_CHAR);
   
    //Print out bar contents
   for(int i = 0; i < barLength;i++){
    
     printf("%c", barvalue[i]); 

   }

    //print out ]
    printf("%c", RIGHT_CHAR);


  }

  //print \r and print out list
  void refresh(){
    std::cout << "\r";
    print();

  }

  //Set bar percent
  void setPercent(int value){
    //Use lambda
    int fill = progressbar::_rlambda(value);

    //Cycle through list
    for(int i = 0; i < barLength; i++){

      //If we have passed the fill value start drawing empty chars (so if value = 20, we draw empty chars from position 3 onwards as 20/10 = 2, which gives our fill range)
       if(i > fill){
         barvalue[i] = EMPTY_CHAR;
         

       } else {
        //Fill if we're below the do-not-fill range
       barvalue[i] = FULL_CHAR;
      }
        
    }
  

  }


  progressBar(){
    for(int i = 0; i < barLength;i++){
       barvalue[i] = EMPTY_CHAR;
    }    

  }

  progressBar(int PercentageValue){
    setPercent(PercentageValue); 
  }


};



};
