#include <cmath>
#include <fstream>
#include <iostream>
#include <mutex>
#include <string>
#include <chrono>
#include <thread>
#include "ProgressBar.hpp"

#define variable(num) num

void sine__(int offset, int run_until);


//Lambda
//
auto CalculatePoints = [](double time, double max_v, double increment){

  return (((max_v/time)/increment*3.14159)*1000);

};


auto getCurrentTime = [](){
  return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()).time_since_epoch());
};





std::mutex thread_;
std::string output;

int DO_FOR = 128;

 unsigned int start = 0;
 unsigned int max = 1000000;

 int increment = 1000000;




int main (int argc, char *argv[]){

  std::cout << "\nPlease enter a value to go up to (in millions): ";
  std::cin >> DO_FOR;



  std::cout << std::endl;

  system("pause");



  progressbar::progressBar BAR(0);

 // double now = std::time(0);
  //
   auto now = getCurrentTime(); 


  for(int i = 0; i <= (DO_FOR);i++){

   
       std::thread _thread(sine__, start, max);
    _thread.join();

    double d_i = i;

    double calculation = ((d_i/DO_FOR)*100);
  
    BAR.setPercent(calculation);
    BAR.refresh();


    start += increment;
    max += increment;
    
    if(max >= ((DO_FOR+1)*increment)){
      break;
    } 

  }

  auto final_t = getCurrentTime(); 

  std::cout << "\nFinished calculating in "
    << "";

  std::cout << "\nPrint calculations to file?\n";
  std::string _dYES;
  std::cin >> _dYES;

  if(_dYES == "yes"){


     long file_write_time = getCurrentTime().count();

     std::ofstream file("sin.txt");


     file << output;

     file.close();

     long final_time_file = getCurrentTime().count();
     std::cout << "\n\nFinished writing data to file in " << final_time_file - file_write_time << "ms";
    

  }

  double calc = final_t.count() - now.count();

  std::cout << "\n\nFinished executing in " << (calc/1000) << "s";

  std::cout << "\nCalculated values of sin ranging from: 0-" << (DO_FOR*increment); 
 
  std::cout << "\n\n\nFinal score: " << int(CalculatePoints((calc/1000), (DO_FOR*increment), increment));

  return 0;
}



void sine__(int offset, int run_until){

  thread_.lock();
  std::string temp_str = "";
 
  for(int num = offset; num < run_until; num++){

     
        temp_str += "Sin of " + std::to_string(num) + " is " + std::to_string(sin(num)) + "\n";

}

    output += temp_str;


  thread_.unlock();

      }
      
